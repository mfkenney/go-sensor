// Unit tests for sensor package
package sensor

import (
	"bytes"
	"testing"
)

func TestCleanData(t *testing.T) {
	line0 := []byte("\x00aaa bbb\tcc, \xff\t\r\n")
	n := len(line0)
	line := bytes.Map(CleanChars, line0)

	if len(line) != (n - 2) {
		t.Errorf("CleanChars failed: %q", line)
	}
}
