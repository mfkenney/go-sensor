// Test data record parsing
package fsi

import (
	"bytes"
	"context"
	"testing"
	"time"
)

type rwbuffer_timeo struct {
	rbuf  *bytes.Buffer
	wbuf  *bytes.Buffer
	delay time.Duration
	count int
}

func (b *rwbuffer_timeo) Read(p []byte) (int, error) {
	b.count++
	if b.count > 1 {
		time.Sleep(b.delay)
	}
	return b.rbuf.Read(p)
}

func (b *rwbuffer_timeo) Write(p []byte) (int, error) {
	return b.wbuf.Write(p)
}

func TestAcmParse(t *testing.T) {
	line := []byte("  29.94,   45.00, -0.4080, -0.9050, -0.1205,  -48.71,   -0.05,  -63.89,   -6.02\r\n")

	f := NewAcm("acm_1", bytes.NewBuffer(line))

	raw, err := f.Recv(context.TODO())
	if err != nil {
		t.Errorf("Error reading data record: %v", err)
	}
	rec, err := f.ParseData(raw)
	v, ok := rec["tx"].(float32)
	if !ok || v < 29.0 || v >= 30.0 {
		t.Errorf("Record parse error: %q", raw)
	}
	v, ok = rec["vd"].(float32)
	if !ok || v <= -7.0 || v > -6.0 {
		t.Errorf("Record parse error: %q", raw)
	}
}

func TestBadRecord(t *testing.T) {
	line := []byte("95\t48\t700\t68\t536\n")

	f := NewAcm("acm_1", bytes.NewBuffer(line))

	raw, err := f.Recv(context.TODO())
	if err != nil {
		t.Errorf("Error reading data record: %v", err)
	}
	rec, err := f.ParseData(raw)
	if err == nil {
		t.Errorf("Bad record not detected: %v", rec)
	}
}

func TestTimeout(t *testing.T) {
	resp := []byte("")
	rw := rwbuffer_timeo{rbuf: bytes.NewBuffer(resp), wbuf: new(bytes.Buffer)}
	rw.delay = time.Second * 1

	f := NewAcm("acm_1", &rw)
	f.SetTimeout(time.Second * 2)

	_, err := f.Recv(context.TODO())
	if err == nil {
		t.Errorf("Timeout not detected!")
	}
}
