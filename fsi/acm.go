// Package provides interfaces to the FSI Acoustic Current Meter
package fsi

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"strconv"
	"time"

	"bitbucket.org/mfkenney/go-sensor"
)

type Acm struct {
	name      string
	rdr       io.Reader
	variables []string
	reclen    int
	trace     sensor.Tracing
}

func NewAcm(name string, port io.ReadWriter) *Acm {
	return &Acm{
		rdr:    port,
		name:   name,
		reclen: 9,
		variables: []string{"tx", "ty",
			"hx", "hy", "hz",
			"va", "vb", "vc", "vd"}}
}

// Set the read timeout
func (dev *Acm) SetTimeout(t time.Duration) {
}

// Acm.Name returns the sensor name
func (dev *Acm) Name() string {
	return dev.name
}

// Acm.SetTrace enables or disables i/o debugging messages
func (dev *Acm) SetTrace(state bool) {
	dev.trace = sensor.Tracing(state)
}

// Send is a no-op
func (dev *Acm) Send(cmd []byte) error {
	return nil
}

// Recv reads the device response
func (dev *Acm) Recv(ctx context.Context) ([]byte, error) {
	reply, err := sensor.ReadUntil(ctx, dev.rdr, '\n')
	dev.trace.Printf("<RECV> %q", reply)
	return reply, err
}

// Sample returns the next raw data record
func (dev *Acm) Sample(ctx context.Context, args ...interface{}) ([]byte, error) {
	return dev.Recv(ctx)
}

// Variables returns the list of data variable names.
func (dev *Acm) Variables() []string {
	v := make([]string, len(dev.variables))
	copy(v, dev.variables)
	return v
}

// ParseData parses a Acm data record from the raw string returned from
// the device. All leading whitespace should be trimmed from rawdata
// before calling this function.
func (dev *Acm) ParseData(rawdata []byte) (rec map[string]interface{}, err error) {
	var x float64
	var f []byte

	rec = make(map[string]interface{})

	fields := bytes.Split(bytes.Map(sensor.CleanChars,
		bytes.Trim(rawdata, " \t\r\n")), []byte(","))
	if len(fields) != dev.reclen {
		err = fmt.Errorf("Bad data record size")
		return
	}

	for i, v := range fields {
		f = bytes.Trim(v, " \t")
		x, err = strconv.ParseFloat(string(f), 32)
		if err != nil {
			return
		}
		rec[dev.variables[i]] = float32(x)
	}

	return
}
