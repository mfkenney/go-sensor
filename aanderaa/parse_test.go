// Test data record parsing
package aanderaa

import (
	"bytes"
	"context"
	"testing"
	"time"
)

type rwbuffer_timeo struct {
	rbuf  *bytes.Buffer
	wbuf  *bytes.Buffer
	delay time.Duration
	count int
}

func (b *rwbuffer_timeo) Read(p []byte) (int, error) {
	b.count++
	if b.count > 1 {
		time.Sleep(b.delay)
	}
	return b.rbuf.Read(p)
}

func (b *rwbuffer_timeo) Write(p []byte) (int, error) {
	return b.wbuf.Write(p)
}

func TestOptodeParse(t *testing.T) {
	line := []byte("4831\t459\t216.884\t23.112\t32.571\t32.571\t41.108\t8.536\t784.8\t636.7\t-74.3\r\n")

	f := NewOptode("optode_1", bytes.NewBuffer(line))

	raw, err := f.Recv(context.TODO())
	if err != nil {
		t.Errorf("Error reading data record: %v", err)
	}
	rec, err := f.ParseData(raw)
	v, ok := rec["t"].(float32)
	if !ok || v < 23.0 || v >= 24.0 {
		t.Errorf("Record parse error: %q", raw)
	}
	v, ok = rec["doconcs"].(float32)
	if !ok || v < 32.0 || v >= 33.0 {
		t.Errorf("Record parse error: %q", raw)
	}
}

func TestBadRecord(t *testing.T) {
	line := []byte("95\t48\t700\t68\t536\n")

	f := NewOptode("optode_1", bytes.NewBuffer(line))

	raw, err := f.Recv(context.TODO())
	if err != nil {
		t.Errorf("Error reading data record: %v", err)
	}
	rec, err := f.ParseData(raw)
	if err == nil {
		t.Errorf("Bad record not detected: %v", rec)
	}
}

func TestTimeout(t *testing.T) {
	resp := []byte("")
	rw := rwbuffer_timeo{rbuf: bytes.NewBuffer(resp), wbuf: new(bytes.Buffer)}
	rw.delay = time.Second * 5

	f := NewOptode("optode_1", &rw)
	f.SetTimeout(time.Second * 2)

	_, err := f.Recv(context.TODO())
	if err == nil {
		t.Errorf("Timeout not detected!")
	}

}
