// Package provides interfaces to Aanderaa sensors
package aanderaa

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"strconv"
	"time"

	"bitbucket.org/mfkenney/go-sensor"
)

// Aanderaa Optode
type Optode struct {
	name      string
	rdr       io.Reader
	variables []string
	fields    []int
	reclen    int
	trace     sensor.Tracing
}

func NewOptode(name string, port io.ReadWriter) *Optode {
	return &Optode{
		rdr:       port,
		name:      name,
		variables: []string{"t", "doconcs"},
		reclen:    11,
		fields:    []int{3, 4}}
}

// Set the read timeout
func (dev *Optode) SetTimeout(t time.Duration) {

}

// Optode.Name returns the sensor name
func (dev *Optode) Name() string {
	return dev.name
}

// Optode.SetTrace enables or disables i/o debugging messages
func (dev *Optode) SetTrace(state bool) {
	dev.trace = sensor.Tracing(state)
}

// Send is a no-op
func (dev *Optode) Send(cmd []byte) error {
	return nil
}

// Recv reads the device response
func (dev *Optode) Recv(ctx context.Context) ([]byte, error) {
	reply, err := sensor.ReadUntil(ctx, dev.rdr, '\n')
	dev.trace.Printf("<RECV> %q", reply)
	return reply, err
}

// Sample returns the next raw data record
func (dev *Optode) Sample(ctx context.Context, args ...interface{}) ([]byte, error) {
	return dev.Recv(ctx)
}

// Variables returns the list of data variable names.
func (dev *Optode) Variables() []string {
	v := make([]string, len(dev.variables))
	copy(v, dev.variables)
	return v
}

// ParseData parses a Optode data record from the raw string returned from
// the device. All leading whitespace should be trimmed from rawdata
// before calling this function.
func (dev *Optode) ParseData(rawdata []byte) (rec map[string]interface{}, err error) {
	var x float64
	var f []byte

	rec = make(map[string]interface{})

	fields := bytes.Split(bytes.Map(sensor.CleanChars,
		bytes.Trim(rawdata, " \t\r\n")), []byte("\t"))
	if len(fields) != dev.reclen {
		err = fmt.Errorf("Bad data record size")
		return
	}

	for i, v := range dev.fields {
		f = bytes.Trim(fields[v], " \t\r\n")
		x, err = strconv.ParseFloat(string(f), 32)
		if err != nil {
			return
		}
		rec[dev.variables[i]] = float32(x)
	}

	return
}
