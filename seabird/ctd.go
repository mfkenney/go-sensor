// Package provides interfaces to Seabird sensors
package seabird

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"strconv"
	"time"

	"bitbucket.org/mfkenney/go-sensor"
)

// Generic Seabird CTD interface
type ctd struct {
	name         string
	port         io.ReadWriter
	variables    []string
	last_command []byte
	trace        sensor.Tracing
	eol          string
	err_reply    []byte
}

type SeabirdError struct {
	cmd string
}

func (e *SeabirdError) Error() string {
	return fmt.Sprintf("Invalid command: %s", e.cmd)
}

// Set the read timeout
func (dev *ctd) SetTimeout(t time.Duration) {
}

// ctd.SetTrace enables or disables i/o debugging messages
func (dev *ctd) SetTrace(state bool) {
	dev.trace = sensor.Tracing(state)
}

// ctd.Send sends a command string to the device.
func (dev *ctd) Send(cmd []byte) error {
	dev.trace.Printf("<SEND> %q", cmd)
	n, err := dev.port.Write(append(cmd, dev.eol...))
	if n > 0 {
		dev.last_command = bytes.ToLower(cmd)
	}
	return err
}

// ctd.Recv returns a command response
func (dev *ctd) Recv(ctx context.Context) ([]byte, error) {
	reply, err := sensor.ReadUntil(ctx, dev.port, '>')
	// Check for error response ...
	if err == nil && bytes.Contains(reply, dev.err_reply) {
		err = &SeabirdError{string(dev.last_command)}
	}
	dev.trace.Printf("<RECV> %q", reply)
	return reply, err
}

// ctd.Variables returns the list of data variable names.
func (dev *ctd) Variables() []string {
	v := make([]string, len(dev.variables))
	copy(v, dev.variables)
	return v
}

// ctd.ParseData parses the raw data to a map
func (dev *ctd) ParseData(rawdata []byte) (rec map[string]interface{}, err error) {
	var (
		x  float64
		ix int64
	)

	rec = make(map[string]interface{})

	fields := bytes.Split(bytes.Map(sensor.CleanChars,
		bytes.Trim(rawdata, " S>\t\r\n")), []byte(","))
	if len(fields) < len(dev.variables) {
		err = fmt.Errorf("Bad data record size: %s", rawdata)
		return
	}

	for i, name := range dev.variables {
		f := bytes.Trim(fields[i], " \t\r\n")
		if bytes.Contains(f, []byte(".")) {
			x, err = strconv.ParseFloat(string(f), 32)
			if err != nil {
				return
			}
			rec[name] = float32(x)
		} else {
			ix, err = strconv.ParseInt(string(f), 0, 32)
			if err != nil {
				return
			}
			rec[name] = int32(ix)
		}
	}
	return
}

// ctd.Name returns the sensor name
func (dev *ctd) Name() string {
	return dev.name
}

func new_ctd(name string, port io.ReadWriter) *ctd {
	return &ctd{
		port:      port,
		name:      name,
		eol:       string("\r"),
		err_reply: []byte("?CMD")}
}
