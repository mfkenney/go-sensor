// Test data record parsing
package seabird

import (
	"bytes"
	"context"
	"testing"
	"time"
)

type rwbuffer_timeo struct {
	rbuf  *bytes.Buffer
	wbuf  *bytes.Buffer
	delay time.Duration
	count int
}

func (b *rwbuffer_timeo) Read(p []byte) (int, error) {
	b.count++
	if b.count > 1 {
		time.Sleep(b.delay)
	}
	return b.rbuf.Read(p)
}

func (b *rwbuffer_timeo) Write(p []byte) (int, error) {
	return b.wbuf.Write(p)
}

func TestCtdParse(t *testing.T) {

	line := []byte("S> 35.4789,  6.9892,  182.25, 3442\r\nS>")
	dev := NewSbe52mp("ctd_1", bytes.NewBuffer(line), true)
	dev.streaming = true

	raw, err := dev.Recv(context.TODO())
	if err != nil {
		t.Errorf("Error reading data record: %v", err)
	}

	rec, err := dev.ParseData(raw)
	if err != nil {
		t.Errorf("Error parsing data record: %v", err)
	}

	if len(rec) != 4 {
		t.Errorf("Bad record length: %d", len(rec))
	}

	_, ok := rec["condwat"].(float32)
	if !ok {
		t.Errorf("Bad type for map value: %v", rec["condwat"])
	}

	_, ok = rec["oxygen"].(int32)
	if !ok {
		t.Errorf("Bad type for map value: %v", rec["oxygen"])
	}

	// No O2 sensor
	line = []byte("\n35.4789,  6.9892,  182.25, 0\r\nS>")
	dev2 := NewSbe52mp("ctd_1", bytes.NewBuffer(line), false)

	raw, err = dev2.Recv(context.TODO())
	if err != nil {
		t.Errorf("Error reading data record: %v", err)
	}

	rec, err = dev2.ParseData(raw)
	if err != nil {
		t.Errorf("Error parsing data record: %v", err)
	}

	if len(rec) != 3 {
		t.Errorf("Bad record length: %d", len(rec))
	}
}

func TestBadRecord(t *testing.T) {
	line := []byte("892,  182.25, 3442\r\nS>")
	dev := NewSbe52mp("ctd_1", bytes.NewBuffer(line), true)

	raw, err := dev.Recv(context.TODO())
	if err != nil {
		t.Errorf("Error reading data record: %v", err)
	}

	rec, err := dev.ParseData(raw)
	if err == nil {
		t.Errorf("Bad record not detected: %v", rec)
	}
}

func TestTimeout(t *testing.T) {
	resp := []byte("\r\n")
	rw := rwbuffer_timeo{rbuf: bytes.NewBuffer(resp), wbuf: new(bytes.Buffer)}
	rw.delay = time.Second * 1

	f := NewSbe52mp("ctd_1", &rw, true)
	f.SetTimeout(time.Second * 2)

	_, err := f.Recv(context.TODO())
	if err == nil {
		t.Errorf("Timeout not detected!")
	}

}
