package seabird

import (
	"context"
	"fmt"
	"io"
)

// Seabird 41 used on MLF2
type Sbe41 struct {
	ctd
}

// NewSbe41 returns a new Sbe41 instance. This type provides an
// interface to the Seabird 41 CTD
func NewSbe41(name string, port io.ReadWriter) *Sbe41 {
	base := new_ctd(name, port)
	base.variables = []string{"tempwat", "salwat"}
	dev := Sbe41{ctd: *base}
	return &dev
}

// Sample returns the next raw data record
func (dev *Sbe41) Sample(ctx context.Context, args ...interface{}) ([]byte, error) {
	pr := args[0].(float32)
	err := dev.Send([]byte(fmt.Sprintf("%05dTS", int(pr*10.))))
	if err == nil {
		_, err = dev.Recv(ctx)
		if err == nil {
			err = dev.Send([]byte("SS"))
			if err == nil {
				return dev.Recv(ctx)
			}
		}
	}
	return nil, err
}
