package seabird

import (
	"context"
	"io"

	sensor "bitbucket.org/mfkenney/go-sensor"
)

// Seabird 52-MP
type Sbe52mp struct {
	ctd
	streaming bool
}

// NewSbe52mp returns a new Sbe52mp instance. This type provides an
// interface to the Seabird 52-MP CTD (with optional oxygen sensor)
func NewSbe52mp(name string, port io.ReadWriter, has_oxy bool) *Sbe52mp {
	base := new_ctd(name, port)
	if has_oxy {
		base.variables = []string{"condwat", "tempwat", "preswat", "oxygen"}
	} else {
		base.variables = []string{"condwat", "tempwat", "preswat"}
	}
	dev := Sbe52mp{ctd: *base, streaming: false}
	return &dev
}

// Send sends a command to the device.
func (dev *Sbe52mp) Send(cmd []byte) error {
	err := dev.ctd.Send(cmd)
	if err == nil {
		// The 'startprofile' command, puts the sensor into
		// streaming mode so the responses terminate with
		// a linefeed rather than the command prompt.
		switch string(cmd) {
		case "startprofile":
			dev.streaming = true
		case "stopprofile":
			dev.streaming = false
		}
	}
	return err
}

// Recv returns the response to the last command or the next raw-data
// sample if the device is in autonomous sampling (streaming) mode.
func (dev *Sbe52mp) Recv(ctx context.Context) ([]byte, error) {
	if dev.streaming {
		return sensor.ReadUntil(ctx, dev.port, '\n')
	}
	return dev.ctd.Recv(ctx)
}

// Sample returns the next raw data record
func (dev *Sbe52mp) Sample(ctx context.Context, args ...interface{}) ([]byte, error) {
	if dev.streaming {
		return dev.Recv(ctx)
	}
	err := dev.Send([]byte("pts"))
	if err != nil {
		return nil, err
	}
	return sensor.ReadUntil(ctx, dev.port, '\n')
}
