package wetlabs

import (
	"bytes"
	"io"
)

type Flntu struct {
	flr
}

func NewFlntu(name string, port io.ReadWriter) *Flntu {
	return &Flntu{flr: flr{
		rdr:       port,
		name:      name,
		variables: []string{"chlaflo", "ntuflo"},
		fields:    []int{3, 5},
		reclen:    7}}
}

func (f *Flntu) ParseData(rawdata []byte) (rec map[string]interface{}, err error) {
	return f.flr.ParseData(bytes.Trim(rawdata, " \t\r"))
}
