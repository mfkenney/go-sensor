package wetlabs

import (
	"bytes"
	"io"
)

type Flcd struct {
	flr
}

func NewFlcd(name string, port io.ReadWriter) *Flcd {
	return &Flcd{flr: flr{
		rdr:       port,
		name:      name,
		variables: []string{"cdomflo"},
		fields:    []int{3},
		reclen:    5}}
}

func (f *Flcd) ParseData(rawdata []byte) (rec map[string]interface{}, err error) {
	return f.flr.ParseData(bytes.Trim(rawdata, " \t\r"))
}
