// Package provides interfaces to Wetlabs sensors
package wetlabs

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"strconv"
	"time"

	"bitbucket.org/mfkenney/go-sensor"
)

// Generic Wetlabs fluorometer
type flr struct {
	name      string
	rdr       io.Reader
	variables []string
	fields    []int
	reclen    int
}

// Set the read timeout
func (dev *flr) SetTimeout(t time.Duration) {

}

// Name returns the sensor name
func (dev *flr) Name() string {
	return dev.name
}

// Send is a no-op
func (dev *flr) Send(cmd []byte) error {
	return nil
}

// Recv reads the device response
func (dev *flr) Recv(ctx context.Context) ([]byte, error) {
	return sensor.ReadUntil(ctx, dev.rdr, '\n')
}

// Variables returns the list of data variable names.
func (dev *flr) Variables() []string {
	v := make([]string, len(dev.variables))
	copy(v, dev.variables)
	return v
}

// Sample returns the next raw data record
func (dev *flr) Sample(ctx context.Context, args ...interface{}) ([]byte, error) {
	return dev.Recv(ctx)
}

// ParseData parses a Fluormeter data record from the raw string
// returned from the device. All leading whitespace should be trimmed
// from rawdata before calling this function.
func (dev *flr) ParseData(rawdata []byte) (rec map[string]interface{}, err error) {
	var ix int64
	var f []byte

	rec = make(map[string]interface{})

	fields := bytes.Split(bytes.Map(sensor.CleanChars, rawdata), []byte("\t"))
	n := len(fields)
	if n != dev.reclen {
		err = fmt.Errorf("Bad data record size")
		return
	}

	for i, v := range dev.fields {
		f = bytes.Trim(fields[v], " \t")
		ix, err = strconv.ParseInt(string(f), 0, 32)
		if err != nil {
			return
		}
		rec[dev.variables[i]] = int16(ix)
	}

	return
}
