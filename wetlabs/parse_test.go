// Test data record parsing
package wetlabs

import (
	"bytes"
	"context"
	"testing"
	"time"
)

type rwbuffer_timeo struct {
	rbuf  *bytes.Buffer
	wbuf  *bytes.Buffer
	delay time.Duration
	count int
}

func (b *rwbuffer_timeo) Read(p []byte) (int, error) {
	b.count++
	if b.count > 1 {
		time.Sleep(b.delay)
	}
	return b.rbuf.Read(p)
}

func (b *rwbuffer_timeo) Write(p []byte) (int, error) {
	return b.wbuf.Write(p)
}

func TestFlrParse(t *testing.T) {
	line := []byte("08/03/07\t11:22:53\t695\t48\t700\t68\t536\n")

	f := NewFlntu("flntu_1", bytes.NewBuffer(line))

	raw, err := f.Recv(context.TODO())
	if err != nil {
		t.Errorf("Error reading data record: %v", err)
	}
	rec, err := f.ParseData(raw)
	v, ok := rec["chlaflo"].(int16)
	if !ok || v != 48 {
		t.Errorf("Record parse error: %v", rec)
	}
	v, ok = rec["ntuflo"].(int16)
	if !ok || v != 68 {
		t.Errorf("Record parse error: %v", rec)
	}
}

func TestBadRecord(t *testing.T) {
	line := []byte("95\t48\t700\t68\t536\n")

	f := NewFlntu("flntu_1", bytes.NewBuffer(line))

	raw, err := f.Recv(context.TODO())
	if err != nil {
		t.Errorf("Error reading data record: %v", err)
	}
	rec, err := f.ParseData(raw)
	if err == nil {
		t.Errorf("Bad record not detected: %v", rec)
	}
}

func TestTimeout(t *testing.T) {
	resp := []byte("")
	rw := rwbuffer_timeo{rbuf: bytes.NewBuffer(resp), wbuf: new(bytes.Buffer)}
	rw.delay = time.Second * 1

	f := NewFlntu("flntu_1", &rw)
	f.SetTimeout(time.Second * 2)

	_, err := f.Recv(context.TODO())
	if err == nil {
		t.Errorf("Timeout not detected!")
	}
}
