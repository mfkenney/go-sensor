// Package provides an interface to a serial-port
// connected sensor.
package sensor

import (
	"bufio"
	"bytes"
	"context"
	"errors"
	"io"
	"log"
	"time"
	"unicode"
)

type SerialSensor interface {
	// Return the sensor name
	Name() string
	// Send sends a command string to the sensor
	Send(cmd []byte) error
	// Recv returns a reply from the sensor
	Recv(ctx context.Context) (reply []byte, err error)
	// SetTimeout sets the timeout for serial port reads
	SetTimeout(t time.Duration)
	// ParseData takes a data record string from the sensor
	// and converts it to a map. The map keys are the data
	// variable names.
	ParseData(rawdata []byte) (map[string]interface{}, error)
	// Variables returns a list of data variable names
	Variables() []string
	// Sample returns a raw data record
	Sample(ctx context.Context, args ...interface{}) ([]byte, error)
}

// Timestamped raw data record
type RawRecord struct {
	// Sensor name
	Name string
	// Data record timestamp
	T time.Time
	// Data record contents
	Data []byte
}

type Tracing bool

func (t Tracing) Printf(format string, args ...interface{}) {
	if t {
		log.Printf(format, args...)
	}
}

func ReadUntil(ctx context.Context, rdr io.Reader, c byte) ([]byte, error) {
	ch := make(chan []byte)
	go func() {
		defer close(ch)
		rdr := bufio.NewReader(rdr)
		b, err := rdr.ReadBytes(c)
		if err == nil {
			ch <- bytes.TrimRight(b, " \t\r\n")
		}
	}()

	select {
	case resp, ok := <-ch:
		if !ok {
			return nil, errors.New("Read error")
		}
		return resp, nil
	case <-ctx.Done():
		break
	}

	return nil, ctx.Err()
}

// Filter function to remove non-printable characters
func CleanChars(r rune) rune {
	if r > 127 || r < 9 {
		return rune(-1)
	}

	if unicode.IsPrint(r) || unicode.IsSpace(r) {
		return r
	}
	return rune(-1)
}

// Start a goroutine to read data samples from a SerialSensor and send the
// raw values to a channel. Any i/o error will cause the goroutine to
// halt. The returned channel will close when the goroutine halts.
//
// This function will not block sending to c: the caller must ensure that
// c has sufficient space to keep up with the expected data rate.
//
func listen(ctx context.Context, sens SerialSensor, buflen int,
	opts ...interface{}) <-chan []byte {
	c := make(chan []byte, buflen)
	go func() {
		defer func() {
			close(c)
			if e := recover(); e != nil {
				log.Print(e)
			}
		}()

		var (
			err error
			raw []byte
		)

		for {
			raw, err = sens.Sample(ctx, opts...)
			if err != nil {
				log.Printf("%s i/o error: %v", sens.Name(), err)
				return
			} else {
				select {
				case <-ctx.Done():
					return
				case c <- raw:
				default:
				}
			}
		}
	}()

	return c
}

// Sampler starts a goroutine which samples a SerialSensor at a specified
// sampling interval and returns a channel which will receive the data
// records. Interval_ms is the sensor sampling interval in milliseconds.
// Setting interval_ms to zero implies that the sensor streams its data
// records.
//
// Any optional args are passed to sens.Sample()
func Sampler(ctx context.Context, sens SerialSensor, interval_ms int, buflen int,
	opts ...interface{}) <-chan *RawRecord {

	c := make(chan *RawRecord, buflen)
	if interval_ms > 0 {
		// Run at the defined interval
		go func() {
			defer close(c)
			var err error
			tick := time.NewTicker(time.Millisecond * time.Duration(interval_ms))
			for {
				select {
				case <-ctx.Done():
					return
				case <-tick.C:
					rec := &RawRecord{Name: sens.Name(), T: time.Now()}
					rec.Data, err = sens.Sample(ctx, opts...)
					if err != nil {
						log.Printf("%#v: sampling error: %v", sens, err)
					} else {
						select {
						case c <- rec:
						default:
							log.Printf("%s: channel full", sens.Name())
						}
					}
				}
			}
		}()
	} else {
		// Run at whatever rate the sensor produces data
		go func() {
			defer close(c)
			var more bool

			source := listen(ctx, sens, buflen, opts...)
			for {
				rec := &RawRecord{Name: sens.Name()}
				select {
				case <-ctx.Done():
					return
				case rec.Data, more = <-source:
					if !more {
						return
					} else {
						rec.T = time.Now()
						select {
						case c <- rec:
						default:
							log.Printf("%s: channel full", sens.Name())
						}
					}
				}
			}
		}()
	}

	return c
}
