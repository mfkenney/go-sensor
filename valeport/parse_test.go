// Test data record parsing
package valeport

import (
	"bytes"
	"context"
	"testing"
	"time"
)

type rwbuffer_timeo struct {
	rbuf  *bytes.Buffer
	wbuf  *bytes.Buffer
	delay time.Duration
	count int
}

func (b *rwbuffer_timeo) Read(p []byte) (int, error) {
	b.count++
	if b.count > 1 {
		time.Sleep(b.delay)
	}
	return b.rbuf.Read(p)
}

func (b *rwbuffer_timeo) Write(p []byte) (int, error) {
	return b.wbuf.Write(p)
}

func TestAltParse(t *testing.T) {
	line := []byte("$PRVAT,00.115,M,0010.073,dBar*39\r\n>")

	alt := NewVa500("alt_1", bytes.NewBuffer(line), true)
	raw, err := alt.Recv(context.TODO())
	if err != nil {
		t.Errorf("Error reading data record: %v", err)
	}
	rec, err := alt.ParseData(raw)
	v, ok := rec["range"].(float32)
	if !ok || v != 0.115 {
		t.Errorf("Record parse error: %v", rec)
	}
	v, ok = rec["pressure"].(float32)
	if !ok || v != 10.073 {
		t.Errorf("Record parse error: %v", rec)
	}
}

func TestTimeout(t *testing.T) {
	resp := []byte("")
	rw := rwbuffer_timeo{rbuf: bytes.NewBuffer(resp), wbuf: new(bytes.Buffer)}
	rw.delay = time.Second * 1

	f := NewVa500("alt_1", &rw, true)
	f.SetTimeout(time.Second * 2)

	_, err := f.Recv(context.TODO())
	if err == nil {
		t.Errorf("Timeout not detected!")
	}
}
