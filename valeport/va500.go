// Interface to Valeport altimeter
package valeport

import (
	"context"
	"fmt"
	"io"
	"strconv"
	"time"

	"bitbucket.org/mfkenney/go-nmea"
	"bitbucket.org/mfkenney/go-sensor"
)

// A Va500 provides a SerialSensor interface to a Valeport VA500
// altimeter. In order to use the ParseData method, the output
// format of the device must be set to NMEA:
//
//     alt := NewVa500(port, true)
//     alt.Send("#082;NMEA")
//
type Va500 struct {
	name      string
	port      io.ReadWriter
	variables []string
	fields    []int
	trace     sensor.Tracing
	eol       string
	streaming bool
}

// NewVa500 returns a new Va500 connected to the specified serial port
func NewVa500(name string, port io.ReadWriter, has_pressure bool) *Va500 {
	variables := []string{"range"}
	fields := []int{0}
	if has_pressure {
		variables = append(variables, "pressure")
		fields = append(fields, 2)
	}

	return &Va500{
		port:      port,
		name:      name,
		variables: variables,
		fields:    fields,
		eol:       string("\r\n"),
		streaming: false,
		trace:     sensor.Tracing(false)}
}

// Set the read timeout
func (dev *Va500) SetTimeout(t time.Duration) {

}

// Name returns the sensor name
func (dev *Va500) Name() string {
	return dev.name
}

// SetTrace enables or disables i/o debugging messages
func (dev *Va500) SetTrace(state bool) {
	dev.trace = sensor.Tracing(state)
}

// Send sends a command string to the device.
func (dev *Va500) Send(cmd []byte) error {
	var err error
	dev.trace.Printf("<SEND> %q", cmd)
	if !dev.streaming {
		_, err = dev.port.Write(append(cmd, dev.eol...))
	} else {
		if string(cmd) == "#" {
			_, err = dev.port.Write(cmd)
			dev.streaming = false
		} else {
			return fmt.Errorf("Command ignored in streaming mode")
		}
	}

	if string(cmd) == "#028" {
		dev.streaming = true
	}

	return err
}

// Recv returns the response to the last command or the
// next raw-data sample if the device is in autonomous sampling
// (streaming) mode.
func (dev *Va500) Recv(ctx context.Context) ([]byte, error) {
	if dev.streaming {
		return sensor.ReadUntil(ctx, dev.port, '\n')
	}
	reply, err := sensor.ReadUntil(ctx, dev.port, '>')
	dev.trace.Printf("<RECV> %q", reply)
	return reply, err
}

// Variables returns the list of data variable names.
func (dev *Va500) Variables() []string {
	v := make([]string, len(dev.variables))
	copy(v, dev.variables)
	return v
}

// ParseData parses the raw NMEA data record and returns a map of
// variable names and values.
func (dev *Va500) ParseData(rawdata []byte) (map[string]interface{}, error) {
	rec := make(map[string]interface{})
	s, err := nmea.ParseSentence(rawdata)
	if err != nil {
		return nil, err
	}

	if len(s.Fields) < len(dev.variables) {
		return nil, fmt.Errorf("Bad data record size")
	}

	for i, v := range dev.fields {
		x, err := strconv.ParseFloat(s.Fields[v], 32)
		if err != nil {
			return nil, err
		}
		rec[dev.variables[i]] = float32(x)
	}

	return rec, nil
}

// Sample returns the next raw data record
func (dev *Va500) Sample(ctx context.Context, args ...interface{}) ([]byte, error) {
	if dev.streaming {
		return dev.Recv(ctx)
	}
	err := dev.Send([]byte("S"))
	if err == nil {
		return dev.Recv(ctx)
	}
	return nil, err
}
