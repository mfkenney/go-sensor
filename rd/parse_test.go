// Test data record parsing
package rd

import (
	"bytes"
	"context"
	"io/ioutil"
	"testing"
	"time"
)

type rwbuffer_timeo struct {
	rbuf  *bytes.Buffer
	wbuf  *bytes.Buffer
	delay time.Duration
	count int
}

func (b *rwbuffer_timeo) Read(p []byte) (int, error) {
	b.count++
	if b.count > 1 {
		time.Sleep(b.delay)
	}
	return b.rbuf.Read(p)
}

func (b *rwbuffer_timeo) Write(p []byte) (int, error) {
	return b.wbuf.Write(p)
}

func TestAdcpParse(t *testing.T) {
	line, err := ioutil.ReadFile("adcptest.hex")
	if err != nil {
		t.Errorf("Error reading test file: %v", err)
	}

	rw := rwbuffer_timeo{rbuf: bytes.NewBuffer(line), wbuf: new(bytes.Buffer)}
	rw.delay = 0

	f := NewAdcp("adcp_1", &rw)

	raw, err := f.Sample(context.TODO())
	if err != nil {
		t.Errorf("Error reading data record: %v", err)
	}

	if len(raw) != (2108 / 2) {
		t.Errorf("Bad record size: %d", len(raw))
	}

	sent := rw.wbuf.String()
	if sent != "ce\rcs\r" {
		t.Errorf("Commands not written: %v", sent)
	}

	rec, err := f.ParseData(raw)
	if rec != nil {
		t.Errorf("Unexpected return from ParseData(): %v", rec)
	}
}

func TestTimeout(t *testing.T) {
	resp := []byte("\r\n")
	rw := rwbuffer_timeo{rbuf: bytes.NewBuffer(resp), wbuf: new(bytes.Buffer)}
	rw.delay = time.Second * 1

	f := NewAdcp("adcp_1", &rw)
	f.SetTimeout(time.Second * 2)

	_, err := f.Recv(context.TODO())
	if err == nil {
		t.Errorf("Timeout not detected!")
	}

}
