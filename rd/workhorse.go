// Package provides an interface to the RD Workhorse ADCP
package rd

import (
	"bytes"
	"context"
	"encoding/hex"
	"io"
	"time"

	"bitbucket.org/mfkenney/go-sensor"
)

type Adcp struct {
	name  string
	port  io.ReadWriter
	trace sensor.Tracing
	eol   string
}

// Return a new sensor data structure.
func NewAdcp(name string, port io.ReadWriter) *Adcp {
	return &Adcp{
		name: name,
		port: port,
		eol:  string("\r"),
	}
}

// SetTrace enables or disables i/o debugging messages
func (dev *Adcp) SetTrace(state bool) {
	dev.trace = sensor.Tracing(state)
}

func (dev *Adcp) SetTimeout(t time.Duration) {
}

// Variables is a no-op for this sensor. The data records are kept
// in their raw form.
func (dev *Adcp) Variables() []string {
	return nil
}

func (dev *Adcp) Send(cmd []byte) error {
	dev.trace.Printf("<SEND> %q", cmd)
	_, err := dev.port.Write(append(cmd, dev.eol...))
	return err
}

// Recv returns the last command response
func (dev *Adcp) Recv(ctx context.Context) ([]byte, error) {
	reply, err := sensor.ReadUntil(ctx, dev.port, '>')
	dev.trace.Printf("<RECV> %q", reply)
	return reply, err
}

// Sample returns the last data ensemble and restarts the sampling
// schedule.
func (dev *Adcp) Sample(ctx context.Context, args ...interface{}) ([]byte, error) {
	err := dev.Send([]byte("ce"))

	if err != nil {
		return nil, err
	}

	data, err := dev.Recv(ctx)
	err = dev.Send([]byte("cs"))

	if len(data) > 0 {
		return hex.DecodeString(
			string(bytes.Map(sensor.CleanChars, bytes.Trim(data, " >\t\r\n"))))
	} else {
		return data, err
	}
}

// ParseData is a no-op for this sensor.
func (dev *Adcp) ParseData(rawdata []byte) (map[string]interface{}, error) {
	return nil, nil
}
